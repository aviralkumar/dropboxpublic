import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './Registration.css';
import Registration from './Registration';
import ReactDOM from 'react-dom';



// Everything in react is a component
// App class extends the component
// render function will render whatever element is there in the return statement

class SignIn extends Component {
	
	state = {
	
			email: '',
			pwd:'',
	}
	
		
	handleClick(e){
		
		
		
		ReactDOM.render(<Registration />, document.getElementById('demo'));
	}
	
  render() {
	  
		
    return (
    		
    		  <div class="SignIn">
    		  <form action="http://localhost:8009/afterSignIn" method="post" class="SignIn">
    		  
    		  
    				  <input placeholder ='email' name='inputUsername' value ={this.state.email} 
    				  onChange={e => this.setState({email: e.target.value})} />
    				  <br/>
    				  <input placeholder ='Password' name='inputPassword' value ={this.state.pwd} 
    				  onChange={e => this.setState({pwd: e.target.value})} />
    				  <br/>
    				  <button onClick={()=> this.onSubmit()}>Submit</button>
    				  
    				  </form>
    				  <div>
    				  <button onClick={this.handleClick.bind(this)}>Register</button>
    				  </div>
    				 </div> 
    				  
    );
  }
}


export default SignIn;

// export makes it available elsewhere
