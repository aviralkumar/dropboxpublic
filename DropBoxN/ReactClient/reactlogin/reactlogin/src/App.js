import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';


// Everything in react is a component
// App class extends the component
// render function will render whatever element is there in the return statement

class App extends Component {
	
	
  render() {
	  
    return (
    		
      <div className="App">
        <div className="App-header">
          <img src="Dropbox2.jpg" className="App-logo" alt="logo" />
          <h2>Welcome to DropBox</h2>
        </div>
        <div id="demo"></div>
        <p className="App-intro">
       
        </p>
      </div>
    );
  }
}


export default App;

// export makes it available elsewhere
