import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './Registration.css';


// Everything in react is a component
// App class extends the component
// render function will render whatever element is there in the return statement

class Registration extends Component {
	
	
	state = {
			
			Fname: '',
			Lname: ' ',
			email: '',
			pwd:'',
	}
			
  render() {
	  	
    return (
    		
    		  <div class="Register">
    		  <form action="http://localhost:8009/afterSignUp" method="post">
    		  
    		  
    				  <input placeholder ='Firstname' name='Fname' value ={this.state.Fname} 
    				  onChange={e => this.setState({Fname: e.target.value})} />
    				  
    				  <br/>
    				  <input placeholder ='lastname' name='Lname' value ={this.state.Lname} 
    				  onChange={e => this.setState({Lname: e.target.value})} />
    				  
    				  <br/>
    				  <input placeholder ='email' name='email' value ={this.state.email} 
    				  onChange={e => this.setState({email: e.target.value})} />
    				  <br/>
    				  <input placeholder ='Password' name='pwd' value ={this.state.pwd} 
    				  onChange={e => this.setState({pwd: e.target.value})} />
    				  <br/>
    				  <button onClick={()=> this.onSubmit()}>Submit</button>
    				  </form>
    				 </div> 
    				  
    );
    
  }
}


export default Registration;

// export makes it available elsewhere
