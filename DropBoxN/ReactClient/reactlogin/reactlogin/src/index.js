
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Registration from './Registration';
import SignIn from './Signin';
import RegisterButton from './RegisterButton';
import registerServiceWorker from './registerServiceWorker';

// ReactDom is the Rendering engine.

// render the app component on to the root element in index.js
ReactDOM.render(<App />, document.getElementById('root'));



ReactDOM.render(<SignIn/>, document.getElementById('demo'));



registerServiceWorker();
