var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var home = require('./routes/home');
var expressValidator = require('express-validator');
var session = require('client-sessions');
var app = express();

var upload = require("express-fileupload");


// all environments
app.set('port', process.env.PORT || 8009);
//Tell Express to render the views from ./views

app.set('views', path.join(__dirname, 'views'));
//Set EJS as the View Engin7
app.set('view engine', 'ejs');
//app.use will be executed everytime an HTTP request is made
app.use(session({   
	cookieName: 'session',    
	secret: 'cmpe273_test_string',    
	duration: 30 * 60 * 10,    //setting the time for active session
	activeDuration: 5 * 60 * 10,  })); // setting time for the session to be active when the window is open // 5 minutes set currently

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.bodyParser());
app.use(upload());
app.use(express.cookieParser());

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', home.signin);
app.get('/signup',home.signup);
app.post('/afterSignUp',home.afterSignUp)
app.get('/afterSignIn',home.signin);
app.get('/failLogin', home.signin);
app.post('/afterSignIn', home.afterSignIn);
app.get('/getAllUsers', home.getAllUsers);

app.get('/afterSignIn/createdir',home.createDirectory);
app.get('/afterSignIn/Files',home.Files);
app.post('/afterSignIn/FileUpload',home.FileUpload);
app.get('/afterSignIn/listfiles',home.listFiles);
app.post('/afterSignIn/Files/Star',home.starFiles);
app.get('/afterSignIn/st',home.listStarFiles);


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
